from django.contrib import admin

from almanac.albums.models import Album, Artist, Genre, Studio, Label, Producer, \
    Track, Favorite


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'link')


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'link')


@admin.register(Studio)
class StudioAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'link')


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'link')


@admin.register(Producer)
class ProducerAdmin(admin.ModelAdmin):
    plist_display = ('id', 'name', 'link')


@admin.register(Track)
class SongAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'album', 'released_date', 'is_single')
    search_fields = ('name',)
    list_filter = ('released_date', 'is_single')


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    list_display = ('id', 'artist', 'name', 'length', 'released_date')
    search_fields = ('name',)
    list_filter = ('released_date',)

@admin.register(Favorite)
class FavoriteAdmin(admin.ModelAdmin):
    pass
