from django.db import models
from .managers import AlbumQuerySet
from almanac.common.utils import generate_filename


class WikiInfo(models.Model):
    name = models.CharField(max_length=255)
    link = models.URLField()

    class Meta:
        abstract = True

    def __str__(self):
        return '{}'.format(self.name)


class Genre(WikiInfo): pass


class Label(WikiInfo): pass


class Studio(WikiInfo): pass


class Producer(WikiInfo): pass


class Artist(WikiInfo): pass


class Album(models.Model):
    name = models.CharField(max_length=255)
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
    genre = models.ForeignKey('Genre', db_index=False,  on_delete=models.CASCADE)
    studio = models.ForeignKey('Studio', db_index=False, on_delete=models.CASCADE)
    label = models.ForeignKey('Label', db_index=False, on_delete=models.CASCADE)
    producers = models.ManyToManyField('Producer')
    cover = models.ImageField(upload_to=generate_filename, null=True, blank=True)
    length = models.DurationField()
    recording_start_date = models.DateField()
    recording_end_date = models.DateField()
    released_date = models.DateField()

    objects = AlbumQuerySet.as_manager()

    def __str__(self):
        return '{}'.format(self.name)

    def get_recorded_range(self):
        return '{} - {}'.format(self.recording_start_date, self.recording_end_date)


class Track(models.Model):
    name = models.CharField(max_length=255)
    album = models.ForeignKey('Album', related_name='tracks', on_delete=models.CASCADE)
    track = models.FileField(upload_to=generate_filename, null=True, blank=True)
    length = models.DurationField()
    released_date = models.DateField(null=True, blank=True)
    is_single = models.BooleanField(default=False)

    def __str__(self):
        return '{} - {}'.format(self.pk, self.name)

    class Meta:
        ordering = ['id']


class Favorite(models.Model):
    album = models.ForeignKey('Album', db_index=False, on_delete=models.CASCADE)
    profile = models.ForeignKey('accounts.UserProfile', db_index=False,
                                on_delete=models.CASCADE)

    class Meta:
        unique_together = ('album', 'profile')
