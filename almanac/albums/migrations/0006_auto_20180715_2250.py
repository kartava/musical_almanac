# Generated by Django 2.0.5 on 2018-07-15 22:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20180704_2357'),
        ('albums', '0005_auto_20180715_2142'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Favorites',
            new_name='Favorite',
        ),
        migrations.AlterUniqueTogether(
            name='favorite',
            unique_together={('album', 'profile')},
        ),
    ]
