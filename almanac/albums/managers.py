from django.db.models.query import QuerySet

class AlbumQuerySet(QuerySet):
    def get_related_objects(self):
        return self.select_related('artist', 'genre', 'studio', 'label') \
            # .prefetch_related('tracks', 'producers')
