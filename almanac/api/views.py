from collections import OrderedDict
from functools import partial

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse


def api_tree(request, format=None):
    """
    API Tree
    """
    r = partial(reverse, request=request, format=format)
    return Response(OrderedDict([
        # ('api-auth', OrderedDict([
        #     ('login', r('rest_login')),
        #     ('logout', r('rest_logout')),
        #     ('registration', r('rest_register')),
        #     ('verify-email', r('rest_verify_email')),
        #     ('password-change', r('rest_password_change')),
        #     ('password-reset', r('rest_password_reset')),
        #     ('password-reset-confirm', r('rest_password_reset_confirm')),
        # ])),
        # ('users', OrderedDict([
        #     ('list', r('api:user-list')),
        #     ('detail', r('api:user-detail', args=[1])),
        # ])),
        ('albums', OrderedDict([
            ('list', r('api:album-list')),
            ('detail', r('api:album-detail', args=[1])),
        ])),
    ]))


api_tree.permission_classes = ()
api_tree = api_view(('GET',))(api_tree)
