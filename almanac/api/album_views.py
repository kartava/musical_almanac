from rest_framework import mixins, status
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from almanac.albums.models import Album, Favorite
from almanac.api.album_serializers import AlbumSerializer, FavoritesSerializer


class AlbumViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                   GenericViewSet):

    serializer_class = AlbumSerializer
    queryset = Album.objects.get_related_objects()


class FavoritesAPIView(APIView):

    def perform_create(self, serializer):
        kwargs = {
            'profile': self.request.user.profile,
            'album': serializer.validated_data['album']
        }
        queryset = Favorite.objects.filter(**kwargs)
        if queryset.exists():
            msg = 'Favorite with this album and profile already exists.'
            raise ValidationError(msg)
        serializer.save(profile=self.request.user.profile)

    def get(self, request):
        profile = self.request.user.profile
        album_ids = profile.favorite_set.values_list('album', flat=True)
        albums = Album.objects.get_related_objects().filter(id__in=album_ids)
        serializer = AlbumSerializer(albums, context={'request': request}, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = FavoritesSerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response({}, status=status.HTTP_201_CREATED)
