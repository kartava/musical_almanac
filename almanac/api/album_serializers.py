from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from almanac.albums.models import Track, Album, Producer, Artist, Genre, Studio, \
    Label, Favorite


class TrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Track
        fields = '__all__'


class ArtistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Artist
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre
        fields = '__all__'



class StudioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Studio
        fields = '__all__'


class LabelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Label
        fields = '__all__'


class ProducerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Producer
        fields = '__all__'

from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
class AlbumSerializer(CachedSerializerMixin):
    artist = ArtistSerializer()
    tracks = TrackSerializer(many=True)
    producers = ProducerSerializer(many=True)
    genre = GenreSerializer()
    studio = StudioSerializer()
    label = LabelSerializer()

    class Meta:
        model = Album
        fields = '__all__'


cache_registry.register(AlbumSerializer)

class FavoritesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Favorite
        fields = ('album',)
