from django.urls import re_path, include
from rest_framework import routers

from almanac.api import views
from almanac.api.album_views import AlbumViewSet, FavoritesAPIView

router = routers.SimpleRouter()
router.register(r'albums', AlbumViewSet,  base_name='album')


urlpatterns = [
    re_path(r'^$', views.api_tree),
    re_path(r'^', include(router.urls)),
    re_path(r'^favorites', FavoritesAPIView.as_view()),
    re_path(r'^auth/', include('rest_auth.urls')),
    re_path(r'^auth/registration/', include('rest_auth.registration.urls')),
]
