from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'almanac.accounts'

    def ready(self):
        from . import signals
